import Vue from "vue";
import Vuex from "vuex";
import Person from "@/models/person.ts";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    persons: new Array<Person>()
  },
  getters: {
    getPersons(state): Person[] {
      return state.persons;
    }
  },
  mutations: {
    setPersons(
      state,
      data: Array<{ id: number; name: string; color: number }>
    ) {
      state.persons = data.map(d => new Person(d));
    }
  },
  actions: {},
  modules: {}
});
