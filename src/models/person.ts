﻿export default class Person {
  public id: number;
  public name: string;
  public color: number;

  constructor(data: { id: number; name: string; color: number }) {
    this.id = data.id;
    this.name = data.name;
    this.color = data.color;
  }
}
