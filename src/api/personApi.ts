import axios from "axios";

export default class PersonApi {
  public getPersons() {
    return axios.get("names.json");
  }
}
